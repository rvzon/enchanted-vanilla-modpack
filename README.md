Welcome to Enchanted Vanilla.

I'll first explain my thought process of how this modpack came to be.
Hopefully, this will help you understand the decisions about the mods that I have included in this modpack.

When creating the modpack I wanted to stay close to the Vanilla experience as possible but still want to have
cool features that enhance the experience and make the world just a bit more interesting to explore.
So while looking at a mod I keep in the back of my mind "Does this feel like Vanilla Minecraft" but
at the same time does this add something to the overall experience without removing the feel of Vanilla Minecraft.

I had to make some consensus to prevent the game from lagging out.

To give an example: For a minimap I decided to go with [Map Atlases](https://modrinth.com/mod/map-atlases) over the other popular map mods unfortunetaly,
after having 3 people with a total of 1000 maps explored the thread just to handle the maps took up 25% of CPU usages because of this I decided to pick [Xaero's Minimap/World Map](https://modrinth.com/mod/xaeros-minimap)

All this is explained, due keep in mind this is a modpack so there are mods in this modpack that will affect how you play the game.

----

## Tech mods

In short, there are no mods that require power to work.
We have [Create](https://modrinth.com/mod/create-fabric) and additional mods (subject to change in the future) such as [Create Utilities]() to help with endgame.

## Progression

This is an interesting one since I did include [LevelZ](https://modrinth.com/mod/levelz) to give an addition than your character.
Every level provides 2 skill points to gain more out of spending your experience points.

Another addition that I added is [Origins](https://modrinth.com/mod/origins) and [Origins: Classes](https://modrinth.com/mod/origins-classes) and a few extensions to this to make it again a bit more interesting. Currently [RPG Origins](https://modrinth.com/datapack/rpg-origins) and [Digs DnD Origins](https://www.curseforge.com/minecraft/mc-mods/digs-dnd-origins).
With a nice [GUI from UltrusBot](https://modrinth.com/mod/altorigingui) to provide a clearer overview of the available Origins.  

## World generation

There are no mods included that will change the Overworld or Nether when it comes to terrain generations, I did include [Endgame Reborn](https://modrinth.com/mod/endgame-reborn) because the end dimension is just really boring.
For more exploration need I selected [Additional Structures](https://modrinth.com/mod/additional-structures) and [ChoiceTheorem's Overhauled Village](https://modrinth.com/mod/ct-overhaul-village) and [Dungeons and Taverns](https://modrinth.com/datapack/dungeons-and-taverns).
For some enhanced experience I included [Falling Leaves](https://modrinth.com/mod/fallingleaves) and
for easier exploration I included [Explorer's Compass](https://www.curseforge.com/minecraft/mc-mods/explorers-compass) and [Useful Backpacks](https://www.curseforge.com/minecraft/mc-mods/useful-backpacks) because well... You know why.

For more animals, we have [Earth2Java](https://modrinth.com/mod/earth2java) and [Living Things](https://modrinth.com/mod/living-things), [Mythic Mounts](https://modrinth.com/mod/mythic-mounts-fabric) because more animals are fun!

## Crops & Foods

For crops and food I picked a few because I like them.
To start [Farmer's Delight](https://modrinth.com/mod/farmers-delight-fabric) because it's just an awesome mod, [Cultural Delights](https://www.curseforge.com/minecraft/mc-mods/cultural-delights-fabric) because I love having more food options, and one that just fits in perfectly [Food+](https://modrinth.com/mod/food-pluss).

Since [Fabric Seasons](https://modrinth.com/mod/fabric-seasons) is also in the modpack with [Delight Compat](https://modrinth.com/mod/fabric-seasons-delight-compat) it's a cool feature to have to take into account or your crops will stop growing. Small tip: Rich dirt will prevent this from happening.

## Magic

For easy traveling to points of interest, [Waystones](https://modrinth.com/mod/waystones) is included.
The only magic mod I included is [Botania](https://modrinth.com/mod/botania) because it's just an awesome mod.

## Honorable mentions

I think the following mods add more live to the game but don't have a specific group I can place them in.

 - [Artifacts](https://modrinth.com/mod/artifacts) (Heavily filtered)
 - [Assorted Storage](https://modrinth.com/mod/assorted-storage) (filtered)
 - [Chipped](https://modrinth.com/mod/chipped)
 - [Copper Equipment](https://modrinth.com/mod/exlines-copper-equipment)
 - [Enchanting Infuser](https://modrinth.com/mod/enchanting-infuser)
 - [Extended Copper](https://modrinth.com/mod/expanded-copper)
 - [GuardVillager](https://modrinth.com/mod/guard-villagers-(fabricquilt))
 - [Handcrafted](https://modrinth.com/mod/handcrafted)
 - [Immersive Armors](https://modrinth.com/mod/immersive-armors)
 - [MC Dungeons Weapons](https://www.curseforge.com/minecraft/mc-mods/mcdw)
 - [Medieval Weapons](https://modrinth.com/mod/medievalweapons)
 - [Mystical Index](https://modrinth.com/mod/mystical-index)
 - [Simple Copper Pipes](https://modrinth.com/mod/simple-copper-pipes)
 - [Supplementaries](https://modrinth.com/mod/supplementaries)
